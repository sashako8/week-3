// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price



// Check
logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 }
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97

const bud = { descr: 'Bud Light', price: 3.99 };
const burger = { descr: 'Hamburger', price: 6.99 };
const logReceipt = (...menuItems) => {
    var subTotal = 0;
    menuItems.forEach(menuItem => {
    console.log(`${menuItem.descr} - ${menuItem.price}`);
    subTotal += menuItem.price;
    })
const tax = subTotal * 0.065;
const total = subTotal + tax;
console.log('Subtotal - ' + subTotal + '\n' + 'Taxes - ' + tax.toFixed(2) + '\n' + 'Total - ' + total.toFixed(2));
};
logReceipt(burger, bud);